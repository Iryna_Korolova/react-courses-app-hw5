import './Button.css';

export default function Button({
	buttonText,
	buttonType = 'button',
	onClick,
	...props
}) {
	return (
		<button className='btn' onClick={onClick} type={buttonType} {...props}>
			{buttonText}
		</button>
	);
}

import './Header.css';
import Logo from './components/Logo/Logo';
import Button from '../../common/Button/Button';

import { useNavigate, useLocation } from 'react-router-dom';
import { useEffect } from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { logIn } from '../../store/user/actionCreators';
import { userSelector } from '../../store/user/selectors';
import { fetchCurrentUser, fetchLogOutUser } from '../../store/user/thunk';

export default function Header() {
	const dispatch = useDispatch();
	const user = useSelector(userSelector);
	const navigate = useNavigate();
	const location = useLocation();

	useEffect(() => {
		const savedToken = window.localStorage.getItem('token');
		if (savedToken) {
			dispatch(logIn(savedToken));
			dispatch(fetchCurrentUser());
		}
	}, []);

	useEffect(() => {
		const signRoutes = ['/login', '/registration'];
		if (
			signRoutes.some((path) => location.pathname.startsWith(path)) &&
			user.isAuth
		) {
			navigate('/courses', { replace: true });
		}
		if (
			!signRoutes.some((path) => location.pathname.startsWith(path)) &&
			!user.isAuth
		) {
			navigate('/login', { replace: true });
		}
	}, [location, navigate, user]);

	function signOut() {
		dispatch(fetchLogOutUser());
		navigate('/login', { replace: true });
	}
	return (
		<header data-testid='header' className='header container'>
			<div className='header-inner'>
				<Logo />
				{user.isAuth && (
					<>
						<div className='header-inner'>
							<h3 className='header-heading'>{user.name || 'Admin'}</h3>
							<Button buttonText='Logout' onClick={signOut} />
						</div>
					</>
				)}
			</div>
		</header>
	);
}

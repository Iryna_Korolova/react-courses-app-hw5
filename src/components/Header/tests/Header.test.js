import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';

import Header from '../Header';

import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

const mockedState = {
	user: {
		isAuth: true,
		name: 'Test Name',
	},
	courses: [],
	authors: [],
};
const mockedStore = {
	getState: () => mockedState,
	subscribe: jest.fn(),
	dispatch: jest.fn(),
};

test('should be in document', async () => {
	render(
		<BrowserRouter>
			<Provider store={mockedStore}>
				<Header />
			</Provider>
		</BrowserRouter>
	);
	const header = screen.getByTestId('header');
	expect(header).toBeInTheDocument();
});

test("should have logo and user's name", async () => {
	render(
		<BrowserRouter>
			<Provider store={mockedStore}>
				<Header />
			</Provider>
		</BrowserRouter>
	);
	const img = screen.getByAltText('logo');
	const name = screen.getByText('Test Name');
	expect(img).toBeInTheDocument();
	expect(name).toBeInTheDocument();
});

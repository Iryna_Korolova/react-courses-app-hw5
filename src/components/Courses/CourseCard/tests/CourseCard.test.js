import CourseCard from './../CourseCard';
import Courses from '../../Courses';

import { render, screen } from '@testing-library/react';

import '@testing-library/jest-dom';

import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

const mockedState = {
	user: {
		isAuth: true,
		name: 'Test Name',
	},
	courses: [
		{
			title: 'Java',
			description: 'Java course',
			duration: 2,
			authors: [
				'9b87e8b8-6ba5-40fc-a439-c4e30a373d36',
				'1c972c52-3198-4098-b6f7-799b45903199',
				'5e0b0f18-32c9-4933-b142-50459b47f09e',
				'9987de6a-b475-484a-b885-622b8fb88bda',
			],
			creationDate: '31/12/1977',
			id: 'f9e63152-fd57-4972-a50d-55da764e2271',
		},
	],
	authors: [
		{ name: 'author', id: '9b87e8b8-6ba5-40fc-a439-c4e30a373d36' },
		{ name: 'author2', id: '1c972c52-3198-4098-b6f7-799b45903199' },
		{ name: 'author3', id: '072fe3fc-e751-4745-9af5-aa9eed0ea9ed' },
		{ name: 'author4', id: '40b21bd5-cbae-4f33-b154-0252b1ae03a9' },
		{ name: 'author5', id: '5e0b0f18-32c9-4933-b142-50459b47f09e' },
		{ name: 'author6', id: '9987de6a-b475-484a-b885-622b8fb88bda' },
	],
};
const mockedStore = {
	getState: () => mockedState,
	subscribe: jest.fn(),
	dispatch: jest.fn(),
};

describe('CourseCard component', () => {
	test('should display title', () => {
		render(
			<BrowserRouter>
				<Provider store={mockedStore}>
					<Courses>
						<CourseCard />
					</Courses>
				</Provider>
			</BrowserRouter>
		);
		const courseCardTitle = screen.getByTestId('course-title');
		expect(courseCardTitle).toBeInTheDocument();
	});

	test('should display description', () => {
		render(
			<BrowserRouter>
				<Provider store={mockedStore}>
					<Courses>
						<CourseCard />
					</Courses>
				</Provider>
			</BrowserRouter>
		);
		const courseCardDescription = screen.getByTestId('course-description');
		expect(courseCardDescription).toBeInTheDocument();
	});

	test('should display duration in the correct format', () => {
		render(
			<BrowserRouter>
				<Provider store={mockedStore}>
					<Courses>
						<CourseCard />
					</Courses>
				</Provider>
			</BrowserRouter>
		);
		const courseCardDuration = screen.getByText('Duration: 00:02 hours');
		expect(courseCardDuration).toBeInTheDocument();
	});

	test('should display authors list', () => {
		render(
			<BrowserRouter>
				<Provider store={mockedStore}>
					<Courses>
						<CourseCard />
					</Courses>
				</Provider>
			</BrowserRouter>
		);
		const courseAuthorsList = screen.getByTestId('authors-list');
		expect(courseAuthorsList).toBeInTheDocument();
	});

	test('should display created date in the correct format', () => {
		render(
			<BrowserRouter>
				<Provider store={mockedStore}>
					<Courses>
						<CourseCard />
					</Courses>
				</Provider>
			</BrowserRouter>
		);
		const courseCreatedDate = screen.getByText('Created: 31.12.1977');
		expect(courseCreatedDate).toBeInTheDocument();
	});
});

import { render, screen } from '@testing-library/react';

import '@testing-library/jest-dom';

import { Provider } from 'react-redux';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { createBrowserHistory } from 'history';

import Courses from '../Courses';
import CourseCard from '../CourseCard/CourseCard';
import userEvent from '@testing-library/user-event';

describe('Courses component', () => {
	const mockedState = {
		user: {
			isAuth: true,
			name: 'Test Name',
			role: 'admin',
		},
		courses: [
			{
				title: 'Java',
				description: 'Java course',
				duration: 2,
				authors: [
					'9b87e8b8-6ba5-40fc-a439-c4e30a373d36',
					'1c972c52-3198-4098-b6f7-799b45903199',
					'5e0b0f18-32c9-4933-b142-50459b47f09e',
					'9987de6a-b475-484a-b885-622b8fb88bda',
				],
				creationDate: '26/05/2022',
				id: 'f9e63152-fd57-4972-a50d-55da764e2271',
			},
			{
				title: 'PHP',
				description: 'PHP course',
				duration: 2,
				authors: [
					'9b87e8b8-6ba5-40fc-a439-c4e30a373d36',
					'1c972c52-3198-4098-b6f7-799b45903199',
				],
				creationDate: '26/05/2022',
				id: '26a74de3-fdb7-4006-9ad3-ea83b1276353',
			},
		],
		authors: [
			{ name: 'author', id: '9b87e8b8-6ba5-40fc-a439-c4e30a373d36' },
			{ name: 'author2', id: '1c972c52-3198-4098-b6f7-799b45903199' },
			{ name: 'author3', id: '072fe3fc-e751-4745-9af5-aa9eed0ea9ed' },
			{ name: 'author4', id: '40b21bd5-cbae-4f33-b154-0252b1ae03a9' },
			{ name: 'author5', id: '5e0b0f18-32c9-4933-b142-50459b47f09e' },
			{ name: 'author6', id: '9987de6a-b475-484a-b885-622b8fb88bda' },
		],
	};
	const mockedStore = {
		getState: () => mockedState,
		subscribe: jest.fn(),
		dispatch: jest.fn(),
	};
	test('should display amount of CourseCard equal length of courses array', () => {
		render(
			<BrowserRouter>
				<Provider store={mockedStore}>
					<Courses>
						<CourseCard />
					</Courses>
				</Provider>
			</BrowserRouter>
		);
		const courseAmount = screen.getAllByTestId('course-card');
		expect(courseAmount.length).toEqual(mockedStore.getState().courses.length);
	});

	test('should display Empty container if courses array length is 0', () => {
		const mockedState = {
			user: {},
			courses: [],
		};
		const mockedStore = {
			getState: () => mockedState,
			subscribe: jest.fn(),
			dispatch: jest.fn(),
		};

		render(
			<BrowserRouter>
				<Provider store={mockedStore}>
					<Courses />
				</Provider>
			</BrowserRouter>
		);
		const container = screen.getByTestId('course-container');
		expect(container).toBeEmptyDOMElement();
	});

	test('CourseForm should be showed after a click on a button "Add new course"', async () => {
		const history = createBrowserHistory();
		history.push('/courses');
		render(
			<BrowserRouter location={history.location} navigator={history}>
				<Provider store={mockedStore}>
					<Routes>
						<Route path='/courses/*' element={<Courses />} />
					</Routes>
				</Provider>
			</BrowserRouter>
		);
		const button = screen.getByTestId('add-course-btn');
		userEvent.click(button);
		const courseForm = screen.getByTestId('course-form');
		expect(courseForm).toBeInTheDocument();
	});
});

import { courseReducer } from '../reducer';

test('should return the initial state', () => {
	expect(courseReducer(undefined, {})).toEqual([]);
});

describe('Course reducer', () => {
	test('should handle ADD_COURSE and returns new state', () => {
		expect(
			courseReducer(
				[
					{
						title: 'Best course',
					},
				],
				{
					type: 'ADD_COURSE',
					payload: {
						title: '2313123',
					},
				}
			)
		).toEqual([
			{
				title: 'Best course',
			},
			{
				title: '2313123',
			},
		]);
	});

	test('should handle SET_ALL_COURSES and returns new state', () => {
		expect(
			courseReducer([], {
				type: 'SET_ALL_COURSES',
				payload: [
					{
						title: 'Java',
						description: 'Java course',
						duration: 2,
					},
				],
			})
		).toEqual([
			{
				title: 'Java',
				description: 'Java course',
				duration: 2,
			},
		]);
	});
});

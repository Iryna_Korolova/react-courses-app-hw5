import { removeCourse, newCourse, updateCourse } from '../../services';
import { deleteCourse, addNewCourse, updateCourses } from './actionCreators';

export function fetchUpdateCourse(courseId, courseUpdate) {
	return (dispatch) => {
		updateCourse(courseId, courseUpdate)
			.then((data) => {
				dispatch(updateCourses(data.result));
			})
			.catch((err) => {
				console.log(err);
			});
	};
}
export function fetchRemoveCourse(courseId) {
	return (dispatch) => {
		removeCourse(courseId)
			.then(() => {
				dispatch(deleteCourse(courseId));
			})
			.catch((err) => {
				console.log(err);
			});
	};
}
export function fetchCreateCourse(courseData) {
	return (dispatch) => {
		newCourse(courseData)
			.then((data) => {
				dispatch(addNewCourse(data.result));
			})
			.catch((err) => {
				console.log(err);
			});
	};
}

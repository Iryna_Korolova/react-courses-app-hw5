import { getCurrentUser, logOutUser, logInUser } from './../../services';
import { setUser, logOut, logIn } from './actionCreators';

export function fetchCurrentUser() {
	return (dispatch) => {
		getCurrentUser()
			.then((user) =>
				dispatch(
					setUser({
						name: user.result.name,
						email: user.result.email,
						role: user.result.role,
					})
				)
			)
			.catch((err) => {
				console.log(err);
			});
	};
}

export function fetchLogInUser(loginData) {
	return (dispatch) => {
		logInUser(loginData)
			.then((data) => {
				dispatch(logIn(data.result));
				window.localStorage.setItem('token', data.result);
				getCurrentUser()
					.then((user) =>
						dispatch(
							setUser({
								name: user.result.name,
								email: user.result.email,
								role: user.result.role,
							})
						)
					)
					.catch((err) => {
						console.log(err);
					});
			})
			.catch((err) => {
				console.log(err);
			});
	};
}

export function fetchLogOutUser() {
	return (dispatch) => {
		logOutUser()
			.then(() => {
				window.localStorage.removeItem('token');
				window.localStorage.removeItem('user');
				dispatch(logOut());
			})
			.catch((err) => {
				console.log(err);
			});
	};
}
